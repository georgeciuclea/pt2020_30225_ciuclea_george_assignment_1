package com.asgn1;
import java.util.LinkedList;

class Polinom {
    private int ordin;
    private LinkedList<Monom> poly;
    Polinom()
    {
        this.poly= new LinkedList<>();
        this.poly.add(new Monom());
        this.ordin=0;
    }
    Polinom(Monom m)
    {
        this.poly= new LinkedList<>();
        this.poly.add(m);
        this.ordin=this.poly.peekFirst().exponent;
    }
    Polinom(String str)
    {
        this.poly= new LinkedList<>();
        this.poly.add(new Monom(0,0));
        parse(str);
    }
    private void parse(String str)
    {
        this.poly.clear();
        this.ordin=0;
        String[] splitArray = str.split("\\s+");
        for (int i=splitArray.length-1;i>=0;i--) {
            this.insertSorted(new Monom(Integer.parseInt(splitArray[i]),splitArray.length-1-i));
        }
    }
    private boolean isnul()
    {
        return this.ordin == 0 && this.poly.peekFirst().coeficient == 0;
    }

    String print()
    {
        StringBuilder str= new StringBuilder();
        for (int i = this.ordin; i >=0 ; i--) {
            Monom t=this.poly.get(i);
            if(t.coeficient==0)
                continue;
            if(t.coeficient>0 && i!=this.ordin)
                str.append("+");
            str.append(t.coeficient);
            if(t.exponent>0) {
                str.append("*X<sup>");
                str.append(i);
                str.append("</sup>");
            }
        }
        return String.valueOf(str);
    }
    void add(Monom m2)
    {
        while(this.poly.getLast().coeficient==0 && this.poly.getLast().exponent!=0)
        {
            this.poly.removeLast();
        }
        this.ordin=this.poly.getLast().exponent;
        if(this.ordin>m2.exponent) {
            this.poly.get(m2.exponent).coeficient += m2.coeficient;
            this.poly.get(m2.exponent).exponent=m2.exponent;
        }
        else if(this.ordin==m2.exponent) {
            this.poly.getLast().coeficient += m2.coeficient;
            while(this.poly.getLast().coeficient==0 && this.poly.getLast().exponent!=0)
            {
                this.poly.removeLast();
                this.ordin--;
            }
        }
        else {
            for (int i = this.ordin + 1; i < m2.exponent; i++) {
                this.poly.addLast(new Monom(0,i));
            }
            this.poly.addLast(m2);
            this.ordin=this.poly.peekLast().exponent;
        }
    }
    private void sub(Monom m2)
    {
        this.add(m2.neg());
    }

    void add(Polinom p2)
    {
        if(this.poly.isEmpty())
        {
            this.poly.add(new Monom(0,0));
        }
        for (Monom m:p2.poly) {
            this.add(m);
        }
        while(this.poly.peekLast().coeficient==0 && this.poly.peekLast().exponent!=0)
        {
            this.poly.removeLast();
        }
    }
    void sub(Polinom p2)
    {
        if(this.poly.isEmpty())
        {
            this.poly.add(new Monom(0,0));
        }
        for (Monom m:p2.poly) {
            this.sub(m);
        }
        while(this.poly.peekLast().coeficient==0 && this.poly.peekLast().exponent!=0)
        {
            this.poly.removeLast();
        }
    }
    void inmultire(Polinom p2)
    {
        Polinom newpoly=new Polinom();
        for (Monom m1:this.poly) {
            for (Monom m2:p2.poly) {
                newpoly.insertSorted(new Monom(m1.coeficient*m2.coeficient,m1.exponent+m2.exponent));
            }
        }
        this.poly=newpoly.poly;
        this.ordin=newpoly.ordin;
    }
    Polinom[] impartire(Polinom p2) {
        Polinom[] polivect;
        polivect = new Polinom[2];
        if (p2.isnul()) {
            polivect[0]=null;
            polivect[1]=null;
            return polivect;
        }
        Polinom c = new Polinom();
        Polinom r = new Polinom();
        for (Monom m:this.poly) {
            r.add(new Monom(m.coeficient,m.exponent));
        }
        r.ordin=this.ordin;
        while(!r.isnul() && r.ordin>=p2.ordin)
        {
            Monom temp=new Monom(r.poly.peekLast().coeficient/p2.poly.peekLast().coeficient,
                                 r.poly.peekLast().exponent-p2.poly.peekLast().exponent);
            c.add(temp);
            Polinom tp=new Polinom();
            for (Monom m:p2.poly) {
                tp.poly.add(new Monom(m.coeficient*temp.coeficient,m.exponent+temp.exponent));
            }
            r.sub(tp);
        }
        polivect[0]=c;
        polivect[1]=r;
        return polivect;
    }

    private void insertSorted(Monom m2) {
        if(m2.exponent>this.ordin)
        {
            this.ordin=m2.exponent;
        }
        if (this.poly.size() == 0) {
            this.poly.add(m2);
        } else if (this.poly.get(0).exponent > m2.exponent) {
            this.poly.add(0, m2);
        } else if (this.poly.get(this.poly.size() - 1).exponent < m2.exponent) {
            this.poly.add(this.poly.size(), m2);
        } else {
            int i = 0;
            while (this.poly.get(i).exponent < m2.exponent) {
                i++;
            }
            if(this.poly.get(i).exponent==m2.exponent) {
                this.poly.get(i).coeficient += m2.coeficient;
                return;
            }
            this.poly.add(i, m2);
        }
    }
}
